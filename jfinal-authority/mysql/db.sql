/*
Navicat MySQL Data Transfer

Source Server         : thing
Source Server Version : 50616
Source Host           : databasejump.mysql.rds.aliyuncs.com:3306
Source Database       : testdb

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2016-03-31 12:14:32
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `system_bug`
-- ----------------------------
DROP TABLE IF EXISTS `system_bug`;
CREATE TABLE `system_bug` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(111) DEFAULT NULL,
  `des` text,
  `type` int(1) DEFAULT NULL COMMENT '类别',
  `createdate` datetime DEFAULT NULL,
  `modifydate` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT '1' COMMENT '1.待解决 2. 已处理 3.忽略',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_bug
-- ----------------------------
INSERT INTO system_bug VALUES ('10', '小Bug1', '<p> 急啊急啊啊啊啊 </p>', '1', '2014-12-03 00:35:15', '2014-12-05 10:14:52', '1');
INSERT INTO system_bug VALUES ('11', 'eee', 'qqqq', '1', '2014-12-11 16:06:25', '2014-12-28 00:09:40', '1');
INSERT INTO system_bug VALUES ('12', '121', '343', '1', '2015-01-16 15:18:58', '2015-01-16 15:18:58', '1');

-- ----------------------------
-- Table structure for `system_log`
-- ----------------------------
DROP TABLE IF EXISTS `system_log`;
CREATE TABLE `system_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `browser` varchar(255) DEFAULT NULL,
  `operation` int(11) DEFAULT '0' COMMENT '1.访问 2 登录 3.添加 4. 编辑 5. 删除',
  `from` varchar(255) DEFAULT NULL COMMENT '来源 url',
  `ip` varchar(22) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_SYSTEM_EVENT` (`uid`) USING BTREE,
  CONSTRAINT `system_log_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `system_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33807 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_log
-- ----------------------------
INSERT INTO system_log VALUES ('33803', null, 'Chrome', '1', 'http://115.29.187.86:8081/', '122.85.59.5', '/jump;JSESSIONID=f45238b7-ef54-4d24-aa01-bf7ab1981aba', '2016-03-31 11:42:34');
INSERT INTO system_log VALUES ('33804', null, 'Chrome', '1', 'http://115.29.187.86:8081/', '122.85.59.5', '/jump', '2016-03-31 11:43:02');
INSERT INTO system_log VALUES ('33805', null, 'Chrome', '1', 'http://115.29.187.86:8081/', '122.85.59.5', '/jump', '2016-03-31 11:43:04');
INSERT INTO system_log VALUES ('33806', null, 'Chrome', '1', 'http://115.29.187.86:8081/', '122.85.59.5', '/jump', '2016-03-31 11:43:12');

-- ----------------------------
-- Table structure for `system_res`
-- ----------------------------
DROP TABLE IF EXISTS `system_res`;
CREATE TABLE `system_res` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `name` varchar(111) DEFAULT NULL,
  `des` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `iconCls` varchar(255) DEFAULT 'am-icon-file',
  `seq` int(11) DEFAULT '1',
  `type` int(1) DEFAULT '2' COMMENT '1 功能 2 权限',
  `modifydate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=154 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_res
-- ----------------------------
INSERT INTO system_res VALUES ('1', null, '系统管理', '系统管理', '#', 'am-icon-desktop', '10', '1', null);
INSERT INTO system_res VALUES ('2', '1', '资源管理', null, '#/system/res', 'am-icon-coffee', '1', '1', null);
INSERT INTO system_res VALUES ('3', '1', '角色管理', null, '#/system/role', 'am-icon-group', '10', '1', null);
INSERT INTO system_res VALUES ('4', '1', '用户管理', null, '#/system/user', 'am-icon-user', '11', '1', null);
INSERT INTO system_res VALUES ('9', '4', '用户删除', null, '/system/user/delete', 'am-icon-fire', '1', '2', null);
INSERT INTO system_res VALUES ('12', '4', '搜索用户', null, '/system/user/serach', 'am-icon-files-o', '1', '2', null);
INSERT INTO system_res VALUES ('18', '2', '资源删除', null, '/system/res/delete', 'am-icon-bookmark-o', '11', '2', null);
INSERT INTO system_res VALUES ('19', '2', '资源保存', null, '/system/res/save', 'am-icon-bookmark', '11', '2', null);
INSERT INTO system_res VALUES ('28', '3', '角色删除', null, '/system/role/delete', 'am-icon-bookmark-o', '11', '2', null);
INSERT INTO system_res VALUES ('29', '3', '角色保存', null, '/system/role/save', 'am-icon-bookmark', '11', '2', null);
INSERT INTO system_res VALUES ('36', '1', '文件上传', null, '/common/file/upload', 'am-icon-download', '20', '2', null);
INSERT INTO system_res VALUES ('63', '4', '冻结用户', null, '/system/user/freeze', 'am-icon-bookmark-o', '11', '2', null);
INSERT INTO system_res VALUES ('146', '4', '用户列表', null, '/system/user/list', 'am-icon-folder', '8', '2', null);
INSERT INTO system_res VALUES ('147', '4', '用户保存', null, '/system/user/save', 'am-icon-folder', '10', '2', null);
INSERT INTO system_res VALUES ('148', '3', '批量删除', null, '/system/res/batchDelete', 'am-icon-folder', '10', '2', null);
INSERT INTO system_res VALUES ('149', '1', '错误日志', null, '#/system/error', 'am-icon-file-text-o', '13', '1', null);
INSERT INTO system_res VALUES ('150', '1', '操作记录', null, '#/system/log', 'am-icon-building-o', '11', '1', null);
INSERT INTO system_res VALUES ('151', null, '首页', '1234', '#/system/', 'am-icon-home', '1', '1', '2015-02-10 16:09:40');

-- ----------------------------
-- Table structure for `system_role`
-- ----------------------------
DROP TABLE IF EXISTS `system_role`;
CREATE TABLE `system_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(55) DEFAULT NULL,
  `des` varchar(55) DEFAULT NULL,
  `seq` int(11) DEFAULT '1',
  `iconCls` varchar(55) DEFAULT 'status_online',
  `pid` int(11) DEFAULT '0',
  `createdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_role
-- ----------------------------
INSERT INTO system_role VALUES ('1', 'admin', '管理员', '1', 'status_online', null, '2015-05-05 14:24:26');
INSERT INTO system_role VALUES ('2', 'user', null, '3', 'status_online', '1', null);
INSERT INTO system_role VALUES ('3', 'guest', '1234', '2', 'status_online', null, null);
INSERT INTO system_role VALUES ('20', '1234', '1234', '1', 'status_online', '0', null);

-- ----------------------------
-- Table structure for `system_role_res`
-- ----------------------------
DROP TABLE IF EXISTS `system_role_res`;
CREATE TABLE `system_role_res` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `res_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_SYSTEM_ROLE_RES_RES_ID` (`res_id`) USING BTREE,
  KEY `FK_SYSTEM_ROLE_RES_ROLE_ID` (`role_id`) USING BTREE,
  CONSTRAINT `system_role_res_ibfk_1` FOREIGN KEY (`res_id`) REFERENCES `system_res` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `system_role_res_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `system_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3223 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_role_res
-- ----------------------------
INSERT INTO system_role_res VALUES ('1760', '1', '2');
INSERT INTO system_role_res VALUES ('1761', '2', '2');
INSERT INTO system_role_res VALUES ('1762', '18', '2');
INSERT INTO system_role_res VALUES ('1763', '19', '2');
INSERT INTO system_role_res VALUES ('1765', '3', '2');
INSERT INTO system_role_res VALUES ('1767', '28', '2');
INSERT INTO system_role_res VALUES ('1768', '29', '2');
INSERT INTO system_role_res VALUES ('1770', '4', '2');
INSERT INTO system_role_res VALUES ('1772', '9', '2');
INSERT INTO system_role_res VALUES ('1774', '12', '2');
INSERT INTO system_role_res VALUES ('1789', '36', '2');
INSERT INTO system_role_res VALUES ('3184', '1', '3');
INSERT INTO system_role_res VALUES ('3185', '2', '3');
INSERT INTO system_role_res VALUES ('3186', '18', '3');
INSERT INTO system_role_res VALUES ('3187', '19', '3');
INSERT INTO system_role_res VALUES ('3188', '1', '1');
INSERT INTO system_role_res VALUES ('3189', '2', '1');
INSERT INTO system_role_res VALUES ('3190', '18', '1');
INSERT INTO system_role_res VALUES ('3191', '19', '1');
INSERT INTO system_role_res VALUES ('3192', '3', '1');
INSERT INTO system_role_res VALUES ('3193', '28', '1');
INSERT INTO system_role_res VALUES ('3194', '29', '1');
INSERT INTO system_role_res VALUES ('3195', '148', '1');
INSERT INTO system_role_res VALUES ('3196', '4', '1');
INSERT INTO system_role_res VALUES ('3197', '9', '1');
INSERT INTO system_role_res VALUES ('3198', '12', '1');
INSERT INTO system_role_res VALUES ('3199', '63', '1');
INSERT INTO system_role_res VALUES ('3200', '146', '1');
INSERT INTO system_role_res VALUES ('3201', '147', '1');
INSERT INTO system_role_res VALUES ('3202', '36', '1');
INSERT INTO system_role_res VALUES ('3203', '149', '1');
INSERT INTO system_role_res VALUES ('3204', '150', '1');
INSERT INTO system_role_res VALUES ('3205', '151', '1');
INSERT INTO system_role_res VALUES ('3206', '1', '20');
INSERT INTO system_role_res VALUES ('3207', '2', '20');
INSERT INTO system_role_res VALUES ('3208', '18', '20');
INSERT INTO system_role_res VALUES ('3209', '19', '20');
INSERT INTO system_role_res VALUES ('3210', '3', '20');
INSERT INTO system_role_res VALUES ('3211', '28', '20');
INSERT INTO system_role_res VALUES ('3212', '29', '20');
INSERT INTO system_role_res VALUES ('3213', '148', '20');
INSERT INTO system_role_res VALUES ('3214', '4', '20');
INSERT INTO system_role_res VALUES ('3215', '9', '20');
INSERT INTO system_role_res VALUES ('3216', '12', '20');
INSERT INTO system_role_res VALUES ('3217', '63', '20');
INSERT INTO system_role_res VALUES ('3218', '146', '20');
INSERT INTO system_role_res VALUES ('3219', '147', '20');
INSERT INTO system_role_res VALUES ('3220', '36', '20');
INSERT INTO system_role_res VALUES ('3221', '149', '20');
INSERT INTO system_role_res VALUES ('3222', '150', '20');

-- ----------------------------
-- Table structure for `system_user`
-- ----------------------------
DROP TABLE IF EXISTS `system_user`;
CREATE TABLE `system_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(55) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  `des` varchar(55) DEFAULT NULL,
  `status` int(1) DEFAULT '1' COMMENT '#1 不在线 2.封号状态 ',
  `icon` varchar(255) DEFAULT 'images/guest.jpg',
  `email` varchar(222) DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `phone` bigint(15) DEFAULT NULL,
  `salt2` varchar(55) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_user
-- ----------------------------
INSERT INTO system_user VALUES ('1', 'admin', '7684a7d4aa0c29681c42503f4edeffba', '流星最帅', '1', 'system/static/i/15.jpg', 'admin@admin.com', null, '1234567', '93ace6bd41a447f078d1292deae9ec53');
INSERT INTO system_user VALUES ('21', '12', '0EECD063CDB4D838E03A56555D86A9AF', '1234', '2', 'system/static/i/5.jpg', '476335667@qq.com', '2015-04-11 15:02:53', '1342342342', null);

-- ----------------------------
-- Table structure for `system_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `system_user_role`;
CREATE TABLE `system_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_SYSTME_USER_ROLE_USER_ID` (`user_id`) USING BTREE,
  KEY `FK_SYSTME_USER_ROLE_ROLE_ID` (`role_id`) USING BTREE,
  CONSTRAINT `system_user_role_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `system_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `system_user_role_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `system_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_user_role
-- ----------------------------
INSERT INTO system_user_role VALUES ('106', '13', '2');
INSERT INTO system_user_role VALUES ('150', '21', '2');
INSERT INTO system_user_role VALUES ('155', '1', '1');
INSERT INTO system_user_role VALUES ('156', null, '20');
