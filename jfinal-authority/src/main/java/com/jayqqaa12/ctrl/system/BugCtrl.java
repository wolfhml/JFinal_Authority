package com.jayqqaa12.ctrl.system;

import com.jayqqaa12.jbase.jfinal.ext.ctrl.JsonController;
import com.jayqqaa12.model.system.Bug;
import com.jayqqaa12.service.system.BugService;
import com.jfinal.ext.route.ControllerBind;

@ControllerBind(controllerKey = "/system/bug")
public class BugCtrl extends JsonController<Bug> {

    private BugService service ;

    
    public void list() {
        sendJson("list", service.findAll());
    }

    public void status() {
        sendJson(getModel().updateAndModifyDate());
    }


    public void save() {
        sendJson(service.save(getModel()));
    }

    public void delete() {
        sendJson(service.deleteById(getPara("id")));
    }

}
