package com.jayqqaa12.ctrl.system;

import javax.mail.MessagingException;

import com.jayqqaa12.jbase.jfinal.ext.ctrl.JsonController;
import com.jayqqaa12.service.EmailService;
import com.jayqqaa12.val.system.EmailExistVal;
import com.jayqqaa12.val.system.EmailVal;
import com.jfinal.aop.Before;
import com.jfinal.ext.route.ControllerBind;

@ControllerBind(controllerKey = "/system/email")
public class EmailCtrl extends JsonController {
	
	private EmailService service = EmailService.service;

	public void sendValEmail() throws MessagingException {

		renderJson(service.sendValEmail(getPara("email")));
	}

	/**
	 * 因为session 超时 前台也无法操作了 所以无需处理错误码
	 */

	@Before(value = { EmailVal.class })
	public void valEmail() {
		renderJson(service.valEmail());

	}

	@Before(value = { EmailExistVal.class })
	public void existEmail() {
		sendJson();
	}

}
