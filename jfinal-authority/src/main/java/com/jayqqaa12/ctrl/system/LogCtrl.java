package com.jayqqaa12.ctrl.system;

import java.io.File;
import java.io.IOException;

import com.jayqqaa12.jbase.jfinal.ext.ctrl.JsonController;
import com.jayqqaa12.jbase.util.Fs;
import com.jayqqaa12.model.json.SendJson;
import com.jayqqaa12.model.system.Log;
import com.jayqqaa12.service.system.LogService;
import com.jfinal.aop.Before;
import com.jfinal.ext.route.ControllerBind;
import com.jfinal.plugin.ehcache.CacheName;
import com.jfinal.plugin.ehcache.EvictInterceptor;

@CacheName(value = "/system/log")
@ControllerBind(controllerKey = "/system/log")
public class LogCtrl extends JsonController<Log> {

	private  LogService service ;


	public void data() {
		sendJson(service.getVisitCount());
	}

	public void browser() {
		sendJson(service.browser());
	}

	public void list() {

		setJsonData("list", service.log(getFrom(Log.dao.TABLENAME)));
		setJsonData("total", service.getCount(getFrom(Log.dao.TABLENAME).getWhere()));
		sendJson();

	}

	public void error() throws IOException {

		String log = Fs.readFile(new File(System.getProperty("LOGDIR") + "/jfinal.log"));
		renderText(log);
	}

	@Before(value = { EvictInterceptor.class })
	public void delete() {
		sendJson(service.deleteById(getPara("id")));
	}

	public void chart() {
		renderGson(service.chart());
	}

}
