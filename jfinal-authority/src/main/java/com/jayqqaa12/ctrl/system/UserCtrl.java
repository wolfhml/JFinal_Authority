package com.jayqqaa12.ctrl.system;

import com.jayqqaa12.service.system.UserService;
import org.apache.shiro.SecurityUtils;

import com.jayqqaa12.common.Consts;
import com.jayqqaa12.jbase.jfinal.ext.ctrl.JsonController;
import com.jayqqaa12.model.system.User;
import com.jayqqaa12.shiro.ShiroCache;
import com.jayqqaa12.val.system.UserValidator;
import com.jfinal.aop.Before;
import com.jfinal.ext.route.ControllerBind;

@ControllerBind(controllerKey = "/system/user")
public class UserCtrl extends JsonController<User> {

	private UserService service ;

	public void list() {
		sendJson("list", service.list(getParaToInt("page"), getParaToInt("count")));
	}

	public void delete() {
		sendJson(service.deleteById(getPara("id")));
	}

	public void freeze() {
		sendJson(service.changeStaus(getParaToInt("id"), getParaToInt("status")));
	}

	@Before(value = { UserValidator.class })
	public void save() {

		sendJson(service.grant(getParaValuesToInt("role_ids"), getModel()));
	}

	public void getUser() {
		sendJson(service.getUser(getPara("id")));
	}

	
	
}
