package com.jayqqaa12.ctrl.system;

import com.jayqqaa12.jbase.jfinal.ext.ctrl.JsonController;
import com.jayqqaa12.model.TreeKit;
import com.jayqqaa12.model.system.Res;
import com.jayqqaa12.service.system.ResService;
import com.jayqqaa12.shiro.ShiroCache;
import com.jayqqaa12.shiro.ShiroInterceptor;
import com.jfinal.ext.route.ControllerBind;

@ControllerBind(controllerKey = "/system/res")
public class ResCtrl extends JsonController<Res> {

	private ResService service ;
	
	

	public void tree() {
		int type = getParaToInt("type", Res.TYPE_MEUE);
		sendJson("tree", service.getTree(getParaToInt("id"), type, getParaToInt("passId")));
	}

	/**
	 * 返回给前端权限控制
	 */
	public void urls() {

		setJsonData("urls", service.getUrls());
		setJsonData("auth_urls", service.getAuthUrls());
		sendJson();
	}

	public void ztree() {
		sendJson("ztree", service.getZTree());
	}

	public void list() {
		Integer pid = getParaToInt("id");
		if (pid != null) setJsonData("parent", service.findById(pid));
		setJsonData("list", service.list(pid));

		sendJson();
	}

	public void delete() {
		sendJson(service.delete(getPara("id")));
	}

	public void batchDelete() {

		sendJson(service.batchDelete(getPara("ids")));
	}

	public void save() {
		
		Res res = getModel();

		sendJson(service.save(res));
	}

}
