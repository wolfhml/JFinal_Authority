package com.jayqqaa12.ctrl.system;

import com.jayqqaa12.jbase.jfinal.ext.ctrl.JsonController;
import com.jayqqaa12.model.TreeKit;
import com.jayqqaa12.model.system.Role;
import com.jayqqaa12.service.system.RoleService;
import com.jayqqaa12.shiro.ShiroCache;
import com.jayqqaa12.val.system.RoleValidator;
import com.jfinal.aop.Before;
import com.jfinal.ext.route.ControllerBind;

@ControllerBind(controllerKey = "/system/role")
public class RoleCtrl extends JsonController<Role> {

	private  RoleService service ;

	public void list() {
		
		renderJson(service.findAll());
	
	}

	@Before(value = { RoleValidator.class })
	public void save() {

		sendJson(service.save(getModel(), getPara("res_ids")));

	}

	public void delete() {

		sendJson(service.delete(getPara("id")));
	}

}
