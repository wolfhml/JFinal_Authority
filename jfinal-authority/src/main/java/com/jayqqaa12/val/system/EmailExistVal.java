package com.jayqqaa12.val.system;

import com.jayqqaa12.common.Code;
import com.jayqqaa12.jbase.jfinal.ext.JsonValidator;
import com.jayqqaa12.model.system.User;
import com.jayqqaa12.service.system.UserService;
import com.jfinal.core.Controller;

public class EmailExistVal extends JsonValidator {

	@Override
	protected void validate(Controller c) {

		if (UserService.me().existEmail(c.getPara("email")) ) addError(Code.EMAIL_EXIT);
	}

}
