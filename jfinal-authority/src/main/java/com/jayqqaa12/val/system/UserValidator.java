package com.jayqqaa12.val.system;

import com.jayqqaa12.common.Code;
import com.jayqqaa12.jbase.jfinal.ext.JsonValidator;
import com.jayqqaa12.service.system.UserService;
import com.jfinal.core.Controller;

public class UserValidator extends JsonValidator {

	@Override
	protected void validate(Controller c) {

		if(c.getPara("user.id")==null&&UserService.me().existEmail(c.getPara("user.email"))){
			addError(Code.EMAIL_EXIT);
		}

	}

}
