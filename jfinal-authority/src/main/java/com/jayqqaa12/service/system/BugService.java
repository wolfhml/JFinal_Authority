package com.jayqqaa12.service.system;

import com.jayqqaa12.jbase.jfinal.auto.BaseService;
import com.jayqqaa12.model.system.Bug;

/**
 * Generated by Jbase.
 */
public class BugService extends BaseService<Bug> {


	@Override
	public boolean save(Bug bug) {
		boolean result =false;

		if (bug.getId() == null) result = bug.saveAndCreateDate();
		else result = bug.update();

		return result;
	}
}
