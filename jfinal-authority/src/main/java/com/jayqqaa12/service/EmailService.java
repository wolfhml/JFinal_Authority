package com.jayqqaa12.service;

import java.util.UUID;

import javax.mail.MessagingException;

import com.jayqqaa12.common.Consts;
import com.jayqqaa12.jbase.sdk.util.ShiroExt;
import com.jayqqaa12.jbase.util.Eml;
import com.jayqqaa12.model.system.User;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;

public class EmailService 
{
	public final static EmailService service = new EmailService();
	private static Log LOG = Log.getLog(EmailService.class);

	
	public boolean valEmail() {

		User u = ShiroExt.getSessionAttr(Consts.SESSION_EAMIL_USER);
		u.encrypt().saveAndCreateDate();
		ShiroExt.setSessionAttr(Consts.SESSION_EAMIL_USER, null);
		
		return true;
	}
	
	

	public    void sendModifyPwdEmail(String email) throws MessagingException
	{
	 
			if(StrKit.isBlank(email))return;
			
			Eml eml = new Eml("smtp.126.com", "wangshuaiby@126.com", "wangshuaiby@126.com", "231566qq");
			eml.addTo(email);
			eml.setSubject("密码修改提示");
			eml.setBody("你最近修改了密码 如非本人操作 请及时 联系管理员 By Jfinal Authority");
			eml.send();
		 

	}
	
	
	
	public    void sendValidatorEmail(String email,String val) throws MessagingException
	{
 
			if(StrKit.isBlank(email))return;
			
			Eml eml = new Eml("smtp.126.com", "wangshuaiby@126.com", "wangshuaiby@126.com", "231566qq");
			eml.addTo(email);
			eml.setSubject("你正在注册用户  输下面的验证码完成验证");
			eml.setBody("验证码为: "+val);
			eml.send();
		 
	}



	public  boolean sendValEmail(String email) throws MessagingException {
		User u = ShiroExt.getSessionAttr(Consts.SESSION_EAMIL_USER);
		if (u != null) email = u.getEmail();

		String uuid = UUID.randomUUID().toString();
		 sendValidatorEmail(email, uuid);
		ShiroExt.setSessionAttr("uuid", uuid);
		
		return true;
	}




}
