package com.jayqqaa12.model.system;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;

import com.jayqqaa12.auto.base.system.BaseUser;
import com.jayqqaa12.common.Consts;
import com.jayqqaa12.jbase.jfinal.ext.util.ListUtil;
import com.jayqqaa12.jbase.sdk.util.ShiroExt;
import com.jayqqaa12.model.TreeKit;
import com.jayqqaa12.shiro.ShiroCache;
import com.jfinal.plugin.activerecord.Db;

public class User extends BaseUser<User> {

    private static final long serialVersionUID = -7615377924993713398L;
    public final static User dao = new User();



    public List<String> getRolesName(String loginName) {

        return TreeKit.getAttr(dao, sql("system_role.getRolesName"), NAME, loginName);
    }
  
    public String getSalt() {
        return getEmail() + getSalt2();
    }

    public User encrypt() {
        String pwd = this.getPwd();

        if (pwd != null) {
            String salt2 = new SecureRandomNumberGenerator().nextBytes().toHex();
            SimpleHash hash = new SimpleHash("md5", pwd, getEmail() + salt2, 2);
            pwd = hash.toHex();
            this.setPwd(pwd);
            this.setSalt2(salt2);
        }

        return this;
    }

    public boolean changeStaus(Integer id, Integer status) {
        if (status == null) return false;
        if (status.equals(1)) status = 2;
        else status = 1;
        return update(STATUS, status, id);
    }

    public User findByEmail(String email) {

        return findFirstByWhere("where email=?", email);
    }

}
