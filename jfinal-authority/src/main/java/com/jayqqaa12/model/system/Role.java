package com.jayqqaa12.model.system;

import com.jayqqaa12.auto.base.system.BaseRole;
import com.jayqqaa12.jbase.jfinal.ext.util.ListUtil;
import com.jayqqaa12.model.Tree;
import com.jayqqaa12.model.TreeKit;
import com.jayqqaa12.shiro.ShiroCache;
import com.jfinal.kit.StrKit;

import java.util.List;
import java.util.stream.Collectors;

public class Role extends BaseRole<Role> {
    private static final long serialVersionUID = -5747359745192545106L;
    public final static Role dao = new Role();

    public List<String> getResUrl(String name) {
        return TreeKit.getAttr(dao, sql("system_res.getResUrl"), "url", name);

    }

    public List<Role> getRole(Object uid) {
        return find(sql("system_role.getRole"), uid);
    }


    public List<Role> getChild(Integer id) {
        if (id == null) return findAllByWhere(" where pid is null order by seq");
        return findAllByWhere(" where pid = ? order by seq ", id);

    }



}
