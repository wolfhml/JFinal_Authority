package com.jayqqaa12.model.system;

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

import com.jayqqaa12.auto.base.system.BaseRes;
import com.jayqqaa12.jbase.sdk.util.ShiroExt;
import com.jayqqaa12.model.Tree;
import com.jayqqaa12.model.TreeKit;
import com.jayqqaa12.model.ZTree;
import com.jfinal.plugin.activerecord.Db;

public class Res extends BaseRes<Res> {
    private static final long serialVersionUID = 9204284399513186930L;
    public final static Res dao = new Res();

    public static final int TYPE_MEUE = 1;
    public static final int TYPE_PERMISSION = 2;




    /***
     * 通过 role id 获得 res
     *
     * @param r
     * @return
     */
    public List<Res> getRes(Object id) {
        return find(sql("system_res.getRes"), id);

    }



}
