package com.jayqqaa12.model.system;

import com.jayqqaa12.auto.base.system.BaseLog;

public class Log extends BaseLog<Log> {
    private static final long serialVersionUID = -128801010211787215L;
    public final static Log dao = new Log();

    public static final int EVENT_VISIT = 1;
    public static final int EVENT_LOGIN = 2;
    public static final int EVENT_ADD = 3;
    public static final int EVENT_DELETE = 4;




}
