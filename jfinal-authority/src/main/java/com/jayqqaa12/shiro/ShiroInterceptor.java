package com.jayqqaa12.shiro;

import java.util.List;

import com.jayqqaa12.jbase.sdk.util.ShiroExt;
import com.jayqqaa12.model.system.Log;
import com.jayqqaa12.model.system.Res;
import com.jayqqaa12.service.system.LogService;
import com.jayqqaa12.service.system.ResService;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;

/***
 * 让 shiro 基于 url 拦截
 * 
 * 主要 数据库中也用url 保存权限
 * 
 * @author 12
 * 
 */
public class ShiroInterceptor implements Interceptor {
	private static ShiroExt ext = new ShiroExt();

	/**
	 * 获取全部 需要控制的权限
	 */
	private static List<String> urls;

	public static void updateUrls() {
		urls = ResService.me().getUrls();
	}

	public void intercept(Invocation ai) {

		if (urls == null) urls = ResService.me().getUrls();

		String url = ai.getActionKey();
		try {

			if (url.contains("delete")) LogService.me().insert(ai.getController(), Log.EVENT_DELETE);
			else if (url.contains("save")) LogService.me().insert(ai.getController(), Log.EVENT_ADD);
			if (urls.contains(url) && !ext.hasPermission(url)) ai.getController().renderError(403);

		} catch (Exception e) {
			ai.getController().renderError(403);
		}

		ai.invoke();

	}

}
